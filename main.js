///From https://raw.githubusercontent.com/mdn/webgl-examples/gh-pages/tutorial/sample2/webgl-demo.js

var width;
var height;
var canvas;
var gl;
var objVerticesBuffer;
var mvMatrix;

var perspectiveMatrix;


var vertexPositionAttributeLambert;

var whiteShader;
var redShader;
var blueShader;
var yellowShader;

// Camera variables
var cameraX;
var cameraY;
var cameraRot;
var cameraRotRate;
var lastX;
var lastY;

// Earth variables
var earthRotRate;
var earthRevRate;
var earthRotTot;
var earthRevTot;

// Moon variables
var moonRotRate;
var moonRevRate;
var moonRotTot;
var moonRevTot;

var monkeyRoot = new Monkey();


//
// start
//
// Called when the canvas is created to get the ball rolling.
// Figuratively, that is. There's nothing moving in this demo.
//
function start() {
    canvas = document.getElementById("glcanvas");

    initWebGL(canvas);      // Initialize the GL context

    // Only continue if WebGL is available and working

    if (gl) {
        gl.clearColor(0.0, 0.0, 0.0, 1.0);  // Clear to black, fully opaque
        gl.clearDepth(1.0);                 // Clear everything
        gl.enable(gl.DEPTH_TEST);           // Enable depth testing
        gl.depthFunc(gl.LEQUAL);            // Near things obscure far things

        // Initialize the shaders; this is where all the lighting for the
        // vertices and so forth is established.

        initShaders();

        // Here's where we call the routine that builds all the objects
        // we'll be drawing.

        initBuffers();

        // Set up to draw the scene periodically.

        setInterval(drawScene, 15);
    }

    // Initialize rotation and revolution variables
    earthRotRate = 0.15;
    earthRevRate = 0.00125;
    earthRotTot = 0.0;
    earthRevTot = 0.0;

    moonRotRate = 0.00349;
    moonRevRate = 0.00349;
    moonRotTot = 0.0;
    moonRevTot = 0.0;

    // Initialize camera variables
    cameraRot = 0.0;
    cameraRotRate = 0.1;
    lastX = 0;
    lastY = 0;
    cameraX = 0;
    cameraY = 0;
}

///Middle mouse wheel handling https://www.sitepoint.com/html5-javascript-mouse-wheel/
document.addEventListener("mousewheel", MouseWheelHandler, false);
// Firefox
document.addEventListener("DOMMouseScroll", MouseWheelHandler, false);

function MouseWheelHandler(e) {
    if ((e.wheelDelta || -e.detail) > 0) {
        cameraRot += cameraRotRate;
    } else {
        cameraRot -= cameraRotRate;
    }
}

document.getElementById('canvas').onmousedown = function (e) {
    lastX = e.pageX;
    lastY = e.pageY;

    document.onmousemove = function (e) {
        updateCamera(e.pageX - lastX, e.pageY - lastY);
        lastX = e.pageX;
        lastY = e.pageY;
    }

    this.onmouseup = function () {
        document.onmousemove = null;
    }
}

function updateCamera(deltaX, deltaY) {
    cameraX -= deltaX;
    cameraY += deltaY;
}

//
// initWebGL
//
// Initialize WebGL, returning the GL context or null if
// WebGL isn't available or could not be initialized.
//
function initWebGL() {
    gl = null;

    try {
        gl = canvas.getContext("experimental-webgl");
    }
    catch(e) {
    }

    // If we don't have a GL context, give up now
    if (!gl) {
        alert("Unable to initialize WebGL. Your browser may not support it.");
    }
}

//
// initBuffers
//
// Initialize the buffers we'll need. For this demo, we just have
// one object -- a simple two-dimensional square.
//
function initBuffers() {
    ///Now do the obj buffer
    objVerticesBuffer = gl.createBuffer();

    gl.bindBuffer(gl.ARRAY_BUFFER, objVerticesBuffer);

    objVertices = getObj(obj).faces;

    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(objVertices), gl.STATIC_DRAW);
}

//
// drawScene
//
// Draw the scene.
//
//Look at https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API/Tutorial/Lighting_in_WebGL
function drawScene() {
    // Update totals
    earthRotTot += earthRotRate;
    earthRevTot += earthRevRate;
    moonRotTot += moonRotRate;
    moonRevTot += moonRevRate;

    // Clear the canvas before we start drawing on it.
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Establish the perspective with which we want to view the
    // scene. Our field of view is 45 degrees, with a width/height
    // ratio of 640:480, and we only want to see objects between 0.1 units
    // and 100 units away from the camera.
    perspectiveMatrix = makePerspective(60, 640.0/480.0, 0.1, 100.0);

    loadIdentity();

    // Handle camera
    mvTranslate([0, 0, -30]);
    mvRotateX(cameraRot);
    mvTranslate([0, 0, 30]);
    mvTranslate([cameraX, cameraY, 0]);

    var currentMonkey = monkeyRoot;

    // Place Sun Monkey
    mvTranslate([0, 0, -30]);

    // Save current matrix (to scale Sun Monkey)
    mvPush();
    mvScale(2.0);

    currentShader = yellowShader;

    // Draw Sun Monkey
    gl.useProgram(currentShader.shaderProgram);
    gl.bindBuffer(gl.ARRAY_BUFFER, objVerticesBuffer);
    gl.vertexAttribPointer(currentShader.attributes["vertexPositionAttribute"], 3, gl.FLOAT, false, 0, 0);
    setMatrixUniforms(currentShader);
    gl.drawArrays(gl.TRIANGLES, 0, objVertices.length / 3);

    // Pop saved matrix (negates scale)
    mvPop();

    // Place Earth Monkey
    mvRotateZ(earthRevTot);
    mvTranslate([0, -8, 0]);
    
    mvPush(); /* - - - Start Moon - - - */

    // Place Moon Monkey
    mvRotateZ(moonRevTot);
    mvTranslate([0, -2, 0]);
    mvScale(0.5);
    //mvRotateZ(moonRotTot);
    
    currentShader = whiteShader;

    // Draw Moon Monkey
    gl.useProgram(currentShader.shaderProgram);
    gl.bindBuffer(gl.ARRAY_BUFFER, objVerticesBuffer);
    gl.vertexAttribPointer(currentShader.attributes["vertexPositionAttribute"], 3, gl.FLOAT, false, 0, 0);
    setMatrixUniforms(currentShader);
    gl.drawArrays(gl.TRIANGLES, 0, objVertices.length / 3);

    mvPop(); /* - - - End Moon - - - */

    mvScale(0.8);
    mvRotateZ(earthRotTot);

    currentShader = blueShader;

    // Draw Earth Monkey
    gl.useProgram(currentShader.shaderProgram);
    gl.bindBuffer(gl.ARRAY_BUFFER, objVerticesBuffer);
    gl.vertexAttribPointer(currentShader.attributes["vertexPositionAttribute"], 3, gl.FLOAT, false, 0, 0);
    setMatrixUniforms(currentShader);
    gl.drawArrays(gl.TRIANGLES, 0, objVertices.length / 3);
}

//
// initShaders
//
// Initialize the shaders, so WebGL knows how to light our scene.
//
function initShaders() {
    whiteShader = (new ShaderProgram("shader-fs-white", "shader-vs", [{localName: "vertexPositionAttribute", shaderName: "aVertexPosition"}]));
    redShader = new ShaderProgram("shader-fs-red", "shader-vs", [{localName: "vertexPositionAttribute", shaderName: "aVertexPosition"}])
    blueShader = new ShaderProgram("shader-fs-blue", "shader-vs", [{localName: "vertexPositionAttribute", shaderName: "aVertexPosition"}])
    yellowShader = new ShaderProgram("shader-fs-yellow", "shader-vs", [{localName: "vertexPositionAttribute", shaderName: "aVertexPosition"}])

    var vertexShaderLambert = getShader(gl, "shader-vs");
    var fragmentShaderLambert = getShader(gl, "shader-fs-red");

    shaderProgramLambert = gl.createProgram();
    gl.attachShader(shaderProgramLambert, vertexShaderLambert);
    gl.attachShader(shaderProgramLambert, fragmentShaderLambert);
    gl.linkProgram(shaderProgramLambert);

    if (!gl.getProgramParameter(shaderProgramLambert, gl.LINK_STATUS)) {
        alert("Unable to initialize the shader program: " + gl.getProgramInfoLog(shader));
    }

    vertexPositionAttributeLambert = gl.getAttribLocation(shaderProgramLambert, "aVertexPosition");
    gl.enableVertexAttribArray(vertexPositionAttributeLambert);
}

//
// getShader
//
// Loads a shader program by scouring the current document,
// looking for a script with the specified ID.
//
function getShader(gl, id) {
    var shaderScript = document.getElementById(id);

    // Didn't find an element with the specified ID; abort.
    if (!shaderScript) {
        return null;
    }

    // Walk through the source element's children, building the
    // shader source string.
    var theSource = "";
    var currentChild = shaderScript.firstChild;

    while(currentChild) {
        if (currentChild.nodeType == 3) {
            theSource += currentChild.textContent;
        }

        currentChild = currentChild.nextSibling;
    }

    // Now figure out what type of shader script we have,
    // based on its MIME type.
    var shader;

    if (shaderScript.type == "x-shader/x-fragment") {
        shader = gl.createShader(gl.FRAGMENT_SHADER);
    } else if (shaderScript.type == "x-shader/x-vertex") {
        shader = gl.createShader(gl.VERTEX_SHADER);
    } else {
        return null;  // Unknown shader type
    }

    // Send the source to the shader object
    gl.shaderSource(shader, theSource);

    // Compile the shader program
    gl.compileShader(shader);

    // See if it compiled successfully
    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        alert("An error occurred compiling the shaders: " + gl.getShaderInfoLog(shader));
        return null;
    }

    return shader;
}

